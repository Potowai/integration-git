""" hello_world is a very simple set of tests for hello_funs """

from hello_funs import say_hello,say_happy_new_year,say_something_to_someone

print(say_something_to_someone("Yo","Bob"))
print(say_hello("Uncle Bob"))
print(say_happy_new_year("Uncle Bob"))
